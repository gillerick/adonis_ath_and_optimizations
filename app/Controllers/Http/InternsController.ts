import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {schema, rules} from '@ioc:Adonis/Core/Validator'
import Intern from 'App/Models/Intern'

export default class InternsController {
  public async index ({}: HttpContextContract) {
    // await auth.use('api').authenticate()
    var interns = await Intern.query().preload('stacks')
    return interns
  }

  public async create ({}: HttpContextContract) {
  }

  public async store ({request}: HttpContextContract) {
    const userRequest = request.only(['name', 'email', 'year', 'stack'])
    
    const internSchema = schema.create({
      name: schema.string(),
      email: schema.string({}, [rules.email()]),
      year: schema.number(),
      stack: schema.string(),
    })

      const stack1 = userRequest.stack[0];
      const stack2 = userRequest.stack[1];
      const stack3 = userRequest.stack[3];
  
      await request.validate({ schema: internSchema });
      const stacks1 = new Intern();
      stack1.name = stack1;
      const stacks2 = new Intern();
      stack2.name = stack2;
      const stacks3 = new Intern();
      stack3.name = stack3;
  
      const newIntern = new Intern();
      newIntern.name = userRequest.name;
      newIntern.email = userRequest.email;
      newIntern.year = userRequest.year;
  
      await newIntern.related('stacks').saveMany([stacks1, stacks2, stacks3]);
      return newIntern;
  
  }

  public async show ({params:{id}}: HttpContextContract) {
    const intern = await Intern.find(id)
    return intern
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({}: HttpContextContract) {
  }

  public async destroy ({params:{id}}: HttpContextContract) {
    const intern = await Intern.find(id)
    await intern?.related('stacks').detach()
    intern?.delete()

    return intern
  }
}
