import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/auths'


export default class UsersController {
    //Sign up user
    public async register({ request}: HttpContextContract) {
        const email = request.input('email')
        const password = request.input('password')
        const user = User.create({
        email:email,
        password:password
        });
        return user
    }


    //Sign in user
    public async login({request, response, auth}: HttpContextContract) {
        const email = request.input('email')
        const password = request.input('password')
        try {
        const token = await auth.use('api').attempt(email, password)
        return token
        } catch {
        return response.badRequest('Invalid user credentials')
        }
    }

  //Logout user
    public async logout({ auth}: HttpContextContract) {
        await auth.use('api').revoke()
        return {
            revoked: true
        }
    }

  
}