import Stack from 'App/Models/Stack'

import {
  column,
  BaseModel,
  manyToMany,
  ManyToMany,

} from '@ioc:Adonis/Lucid/Orm'


export default class Intern extends BaseModel {
  public static table = "interns"

  @column({isPrimary: true})
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column()
  public year: number
  
  @manyToMany(() => Stack,{
    localKey: 'id',
    pivotForeignKey: 'intern_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'stack_id',
  })
  public stacks: ManyToMany<typeof Stack>
}