// import Intern from 'App/Models/Intern'

import {
  column,
  BaseModel,
  // manyToMany,
  // ManyToMany,

} from '@ioc:Adonis/Lucid/Orm'


export default class Stack extends BaseModel {
public static table = "stacks"

  @column({isPrimary: true})
  public id: number

  @column()
  public name: string  

  // @manyToMany(() => Intern,{
  //   localKey: 'id',
  //   pivotForeignKey: 'stack_id',
  //   relatedKey: 'id',
  //   pivotRelatedForeignKey: 'intern_id',
  // })
  // public stacks: ManyToMany<typeof Intern>
}