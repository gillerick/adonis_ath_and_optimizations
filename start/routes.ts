import 'App/Controllers/Http/InternsController'
import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.get('/interns', 'InternsController.index')

Route.get('/interns/:id', 'InternsController.show')

Route.delete('/interns/:id', 'InternsController.destroy')

Route.post('/interns', 'InternsController.store')