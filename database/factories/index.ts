import Intern from 'App/Models/Intern'
import Stack from 'App/Models/Stack'
import Factory from '@ioc:Adonis/Lucid/Factory'

const _stacks = ["Flutter", "Adonis JS", "Golang", "Vue JS", "Quarkus"]
const cohort_years = [2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020]

export const StackFactory = Factory
  .define(Stack, ({faker}) => {
    return {
      name: faker.random.arrayElement(_stacks),
    }
  }).build()

export const InternFactory = Factory
  .define(Intern, ({faker}) => {
    return {
      name: faker.internet.userName(),
      email: faker.internet.email(),
      year: faker.random.arrayElement(cohort_years),
    }
  })
  .relation('stacks', () => StackFactory)
  .build()