import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Interns extends BaseSchema {
  protected tableName = 'interns'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('name').notNullable()
      table.string('email').notNullable()
      table.integer('year')
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
