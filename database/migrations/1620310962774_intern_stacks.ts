import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class InternStacks extends BaseSchema {
  protected tableName = 'intern_stack'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.integer('intern_id').unsigned().references('interns.id')
      table.integer('stack_id').unsigned().references('stacks.id')
  })
}

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
